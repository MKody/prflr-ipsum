# Prflr-Ipsum
## Le "Lorem-Ipsum" blbl

Petit Ipsum marrant. L'idée est venu de [xNuance](https://twitter.com/xNuance) et [ShinigPardox](https://twitter.com/ShiningParadox) sur Twitter :3

**Il a encore du boulot a faire !**

### Todo:
- Moins de mot dans action par rapport au blabla ?
- Plus d'irrégularité dans les paragraphes.
- Plus de mots, meilleure sélection.

Sources des images utilisés dans le header:

- [balloons](http://cl0setbr0ny.deviantart.com/art/Fluffle-Puff-Balloons-378921568) 
- [box](http://chanceh96.deviantart.com/art/Fluffle-Puff-In-A-Box-397787195) 
- [faceplant](http://stewartisme.deviantart.com/art/Fluffle-Pony-Faceplant-Commission-361169014) 
- [fluffle](http://mixermike622.deviantart.com/art/FLUFFLE-PUFF-277414155) 
- [jump](http://stewartisme.deviantart.com/art/Fluffle-Jump-Commission-361170256) 
- [princess](http://youki506.deviantart.com/art/Princess-Fluffle-Puff-358734183) 
- [red_marker](http://stealth1546.deviantart.com/art/Fluffle-Puff-Red-Marker-428786280) 
- [secretary](http://cl0setbr0ny.deviantart.com/art/Fluffle-Puff-Secretary-446424915) 
- [special_somepony](http://chanceh96.deviantart.com/art/Fluffle-Puff-Special-Somepony-424123242) 
- [superpftfhtfp](http://masemj.deviantart.com/art/I-m-Superpftfhtfp-430614678) 
- [dancing](http://mactavish1996.deviantart.com/art/Fluffle-Puff-dancing-on-Rainbow-Dash-383803538)
- [mirror_ponies](http://cl0setbr0ny.deviantart.com/art/Mirror-Ponies-436027819)
- [snow](http://jailboticus.deviantart.com/art/Fluffle-in-the-snow-423682055)
- [opc](http://jailboticus.deviantart.com/art/Fluffle-Puff-POMFs-Clestia-414843873)
- [open_up](http://jailboticus.deviantart.com/art/Open-Up-Fluffle-Puff-400180782)