function randImg(){
    //  les images
    var pic=new Array()

    pic[0]="pretty-crissy.gif"
    pic[1]="balloons.png"
    pic[2]="box.png"
    pic[3]="faceplant.png"
    pic[4]="fluffle.png"
    pic[5]="jump.png"
    pic[6]="princess.png"
    pic[7]="red_marker.png"
    pic[8]="secretary.png"
    pic[9]="special_somepony.png"
    pic[10]="superpftfhtfp.png"
    pic[11]="dancing.png"
    pic[12]="mirror_ponies.gif"
    pic[13]="snow.gif"
    pic[14]="opc.gif"
    pic[15]="open_up.gif"
    pic[16]="doubled_funcave.gif"
    pic[17]="uwot.gif"
    pic[18]="plz.png"
    pic[19]="dr-pomf-symptoms.jpg"
    pic[20]="dr-pomf-couch.jpg"
    pic[21]="dr-pomf-elevator.jpg"
    pic[22]="dr-pomf-solution.gif"
    pic[23]="my-pillow.gif"
    pic[24]="NO-my-pillow.gif"
    pic[26]="double.gif"
    pic[27]="sweets.gif"
    pic[28]="heavy-breathing.jpg"
    pic[29]="monopoly.gif"
    pic[30]="eqg-scissors.gif"
    pic[31]="eqg-sexy.jpg"
    pic[32]="tea-time.gif"

    // lien vers l'image source
    var lienpic=new Array()

    lienpic[0]="https://www.youtube.com/watch?v=eyv1BipQI9w"
    lienpic[1]="http://cl0setbr0ny.deviantart.com/art/Fluffle-Puff-Balloons-378921568"
    lienpic[2]="http://chanceh96.deviantart.com/art/Fluffle-Puff-In-A-Box-397787195"
    lienpic[3]="http://stewartisme.deviantart.com/art/Fluffle-Pony-Faceplant-Commission-361169014"
    lienpic[4]="http://mixermike622.deviantart.com/art/FLUFFLE-PUFF-277414155"
    lienpic[5]="http://stewartisme.deviantart.com/art/Fluffle-Jump-Commission-361170256"
    lienpic[6]="http://youki506.deviantart.com/art/Princess-Fluffle-Puff-358734183"
    lienpic[7]="http://stealth1546.deviantart.com/art/Fluffle-Puff-Red-Marker-428786280"
    lienpic[8]="http://cl0setbr0ny.deviantart.com/art/Fluffle-Puff-Secretary-446424915"
    lienpic[9]="http://chanceh96.deviantart.com/art/Fluffle-Puff-Special-Somepony-424123242"
    lienpic[10]="http://masemj.deviantart.com/art/I-m-Superpftfhtfp-430614678"
    lienpic[11]="http://mactavish1996.deviantart.com/art/Fluffle-Puff-dancing-on-Rainbow-Dash-383803538"
    lienpic[12]="http://cl0setbr0ny.deviantart.com/art/Mirror-Ponies-436027819"
    lienpic[13]="http://jailboticus.deviantart.com/art/Fluffle-in-the-snow-423682055"
    lienpic[14]="http://jailboticus.deviantart.com/art/Fluffle-Puff-POMFs-Clestia-414843873"
    lienpic[15]="http://jailboticus.deviantart.com/art/Open-Up-Fluffle-Puff-400180782"
    lienpic[16]="https://derpibooru.org/735701"
    lienpic[17]="http://askflufflepuff.tumblr.com/post/108092748515/streaming-again-future-animation-spoilers-etc"
    lienpic[18]="https://www.youtube.com/watch?v=lplUHZEaPrM"
    lienpic[19]="https://www.youtube.com/watch?v=oY3M6tDv4pM"
    lienpic[20]="https://www.youtube.com/watch?v=oY3M6tDv4pM"
    lienpic[21]="https://www.youtube.com/watch?v=oY3M6tDv4pM"
    lienpic[22]="https://www.youtube.com/watch?v=oY3M6tDv4pM"
    lienpic[23]="https://www.youtube.com/watch?v=J9FiRDOL4Ow"
    lienpic[24]="https://www.youtube.com/watch?v=J9FiRDOL4Ow"
    lienpic[26]="https://www.youtube.com/watch?v=J9FiRDOL4Ow"
    lienpic[27]="https://www.youtube.com/watch?v=eyv1BipQI9w"
    lienpic[28]="https://www.youtube.com/watch?v=soPYEfY3yRY"
    lienpic[29]="https://www.youtube.com/watch?v=soPYEfY3yRY"
    lienpic[30]="https://www.youtube.com/watch?v=soPYEfY3yRY"
    lienpic[31]="https://www.youtube.com/watch?v=soPYEfY3yRY"
    lienpic[32]="https://www.youtube.com/watch?v=eyv1BipQI9w"

    // random
    var p=Math.floor(Math.random()*pic.length)

    // et on renvoie la pic
    document.write('<a href="'+lienpic[p]+'" target="_blank"><img src="img/'+pic[p]+'" height="180px" alt="*squee*"></a><br><small>Rafraîchissez pour une autre image random</small>')
}
