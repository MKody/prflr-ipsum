$(document).ready(function(){

    $("#pomf-form").submit(function() {
        var paragraphes = '';
        var choix = $("#pomf-form input[name='choix']:checked").val();
        var nb_paragraphe = $("#nb_paragraphes").val();
        if (nb_paragraphe > 100 && nb_paragraphe < 9001) {
            $("#resultat").empty().html('<big>OVER 100?<br>LOL NOPE.</big>');
            return false;
        } else if (nb_paragraphe > 9000) {
            $("#resultat").empty().html('<big>IT\'S OVER 9000!</big>');
            return false;
        } else if (nb_paragraphe == 0) {
            $("#resultat").empty().html('<big>Prflr.</big>');
            return false;
        } else if (nb_paragraphe < 0) {
            $("#resultat").empty().html('<big class="woop">Prflr.</big>');
            return false;
        }
        var mots = [];

        var mots_blabla = ["prflr", "pff", "blbl", "pfrl", "prlf", "bllllbl", "pffrrll", "blbllbl",
            "pflblbl", "bl", "blblbl", "blllbbbll", "blb", "blbbl", "prff", "pffffr", "pfrppf",
            "ppfbllblff", "pfbl", "ppfffbl", "blblpff", "pfrrl", "blblrffrr", "prrflrfflr"];
        var mots_action = ["*squee*", "*boop*", "*breath*", "brrr", "vroummm", "prrrrr",
            "*gasp*", "*pomf*", "grrr"];
        var mots_tout = mots_blabla.concat(mots_action);

        if (choix == "tout") {
            mots = mots_tout;
        } else if (choix == "blabla") {
            mots = mots_blabla;
        } else {
            mots = mots_action;
        }

        for ( var z = 0; z < nb_paragraphe; z++ ) {
            var groupe_phrase = '';
            var nb_phrase = Math.floor( (Math.random()+1) * (Math.random()+1) * (Math.random()+1) );

            function huehuehue(mots) {
                var i = mots.length, j, tempi, tempj;
                if ( i == 0 ) return false;

                while ( --i ) {
                    j = Math.floor( Math.random() * ( i + 2 ) );
                    tempi = mots[i];
                    tempj = mots[j];
                    mots[i] = tempj;
                    mots[j] = tempi;
                }

                return mots;
            }

            for ( var y = 0; y < nb_phrase; y++ ) {

                for ( var x = 0; x < mots.length; x++ ) {

                    var mots_random = huehuehue(mots);
                    var phrase = mots_random.toString().replace(/,/g, ' ') + '. ';

                    function majPremierMot(string) {
                        return string.charAt(0).toUpperCase() + string.slice(1);
                    }

                    var phrase_maj = majPremierMot(phrase);
                }

                groupe_phrase += phrase_maj;
            }

            paragraphes+='<p>' + groupe_phrase + '</p>';
        }

        $("#resultat").empty().html(paragraphes);

        return false;

    });

});
